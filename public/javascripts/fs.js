/* preview is the div where content will be shown */
const preview = document.getElementById('preview');

function handleFiles(files) {
    if(preview.firstChild){
        preview.removeChild(preview.firstChild);
    }
    if (files[0]) {
        const file = files[0];
        if (file.type.startsWith('image/')) {
            const img = document.createElement("img");
            img.classList.add("obj");
            img.style.maxWidth = "400px";
            img.file = file;
            preview.appendChild(img);
            const reader = new FileReader();
            reader.onload = (function (aImg) {
                return function (e) {
                    aImg.src = e.target.result;
                };
            })(img);
            reader.readAsDataURL(file);
        }
    }
}
