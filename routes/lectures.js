var express = require('express');
var Drawing = require('../models/drawing');
var Lecture = require('../models/lecture');
var authenticate = require('../middleware/authenticate');
var router = express.Router();

/* INDEX:  GET lecturs listing. */
router.get('/', function(request, response) {
    // find the list of all lectures
    Lecture.find({}, function (error, list) {
        if (error){
            request.flash('error', "Error searching for lectures");
            response.redirect('/');
        } else {
            response.render('lectures/index', {lectures: list});
        }
    });
});
/* NEW: GET lecture form */
router.get("/new", authenticate.isLoggedIn, function(request, response){
        response.render("lectures/new");
});

/* CREATE: POST lecture form that creates a new lecture */
router.post('/new', authenticate.isLoggedIn, function (request,response) {

        if (request.files.background_file.data){
            data = request.files.background_file.data;
        } else {
            // add something as default
            // currently uploading an image is required.
        }

        /*@TODO: Buffer constructor is deprecated and unsafe,
        *   change the code to use Buffer.alloc(size, string, encoding) */

        var background_file = new Buffer( data ).toString('base64');
        var newLecture = {name: request.body.name,
                          notes: request.body.notes ,
                          author: request.user._id ,
                          background_file: background_file,
                        };
        Lecture.create(newLecture, function (error) {
            if(error){
                request.flash('error', "Could not create the lecture");
                response.redirect("/lectures/new");
            } else {
                //redirect back to index
                response.redirect("/lectures");
            }
        });
});

/* SHOW: GET a lecture */
router.get("/:id", function (request, response) {
    Drawing.find({lecture: request.params.id}, function (error, foundDrawing) {
        if (error) {
            console.log(error);
            request.flash('error', 'could not find the drawing');
            response.redirect('/lectures');
        } else {
            var stringDrawingHistory = [];
            if (foundDrawing.length > 0) {
                stringDrawingHistory = JSON.stringify(foundDrawing[0].events);
            }
            Lecture.findById(request.params.id).exec(function (error, foundLecture) {
                if (error) {
                    console.log(error);
                    request.flash('error', 'could not find the lecture');
                    response.redirect('/lectures');
                } else {
                    response.render('lectures/show', {lecture: foundLecture, drawing: stringDrawingHistory});
                }
            });
        }
    });
});

/* SHOW: GET live white board */
router.get("/:id/live", function (request, response) {
    Lecture.findById(request.params.id).exec(function (error, foundLecture){
        if(error){
            console.log(error);
            request.flash('error', 'could not find the lecture');
            response.redirect('/lectures');
        } else {
            response.render('lectures/live', {lecture : foundLecture});
        }
    })
});

/* EDIT: GET the edit page */
router.get("/:id/edit", authenticate.lectureOwnership, function(request, response){
    Lecture.findById(request.params.id, function (error, foundLecture) {
        response.render('lectures/edit', {lecture : foundLecture});
    });
});
/* UPDATE: PUT the edit page */
router.put('/:id', authenticate.lectureOwnership, function (request, response) {
    if(request.body.deleteLineHistory === 'true'){
        Drawing.deleteMany({lecture: request.params.id}, function (error) {
            if(error) {
                request.flash('error', 'Could not find drawings of the lecture');
            } else {
                console.log("deleted successfully.");
            }
        });
    } else {
        console.log('Continue ...');
    }
    /* If a new file has been added, update file first */
    if(request.files){
        Lecture.findByIdAndUpdate(request.params.id, {
            background_file: new Buffer(request.files.background_file.data).toString('base64')
        }, function (error) {
            if(error) {
                request.flash('error', 'Could not update the slide image.');
            } else {
                console.log('no error after updating image.');
                request.flash('message', 'Slide image was updated');
            }
        });
    } else {
        console.log('no file change, continue ...');
    }
    /* Update the rest, and redirect */
    Lecture.findByIdAndUpdate(request.params.id,{
        name: request.body.name,
        notes: request.body.notes,

    }, function (error) {
        if (error){
            request.flash('error', 'Could not update the lecture info.');
            response.redirect("/lectures/" + request.params.id)
        } else {
            request.flash('message', 'Lecture is updated.');
            response.redirect("/lectures/" + request.params.id);
        }
    });
});

router.post("/:id/lineHistory", authenticate.lectureOwnership, function(request, response) {
    if(request.body['historyInput']){
        var lineHistory = JSON.parse(request.body["historyInput"]);
    }
    //finds drawing based lecture ID and user ID. If found, push new history into events. If not found, create new model
    if(lineHistory){
        Drawing.findOneAndUpdate({lecture: request.params.id}, {$push: {events: {$each: lineHistory}}},  function (error, object) {
            if(error){
                request.flash('error', "bad update");
            } else {
                if (object == null) {
                    //target not found, create new drawing model
                    var newDrawing = {
                        author: request.user._id,
                        lecture: request.params.id,
                        events: lineHistory
                    };
                    Drawing.create(newDrawing, function (error) {
                        if(error) {
                            request.flash('error', "Could not create drawing");
                        } else {
                            response.redirect("/lectures/" + request.params.id);
                        }
                    });
                } else {
                    response.redirect("/lectures/" + request.params.id);
                }
            }
        });
    } else {
        request.flash('message', 'Nothing was added!!');
        response.redirect('/lectures/' + request.params.id);
    }
});

/* DESTROY: DELETE a lecture*/
router.delete("/:id", authenticate.lectureOwnership, function (request, response) {
    Drawing.deleteOne({lecture: request.params.id}, function (error) {
        if (error) {
            console.log(error);
            request.flash('error', 'could not find the drawing');
            response.redirect('/lectures');
        } else {
            Lecture.findById(request.params.id, function () {
                Lecture.findByIdAndDelete(request.params.id, function (error) {
                    if (error) {
                        request.flash('error', 'could not delete the lecture');
                        response.redirect("/lectures");
                    } else {
                        response.redirect("/lectures");
                    }
                });
            });
        }
    });
});

module.exports = router;
